# Utask - Task Management App

Utask is a task management app developed using Flutter. It provides a simple and intuitive interface for users to manage their tasks efficiently. The app offers features such as dynamic greetings based on the time of the day, a floating action button for adding new tasks, a task list sorted by the date of completion, options for editing and deleting tasks, and support for both light and dark modes.

## Screenshots

<img src="https://gitlab.com/hasan082/task-management-ostad/-/raw/main/splashscreen.png" alt="" width="220">
    <img src="https://gitlab.com/hasan082/task-management-ostad/-/raw/main/taskscreen.png" alt="" width="220">
    <img src="https://gitlab.com/hasan082/task-management-ostad/-/raw/main/taskNight.png" alt="" width="220">
    <img src="https://gitlab.com/hasan082/task-management-ostad/-/raw/main/taskadd.png" alt="" width="220">
    <img src="https://gitlab.com/hasan082/task-management-ostad/-/raw/main/listadded.png" alt="" width="220">
    <img src="https://gitlab.com/hasan082/task-management-ostad/-/raw/main/listdone.png" alt="" width="220">
    <img src="https://gitlab.com/hasan082/task-management-ostad/-/raw/main/listdelete.png" alt="" width="220">

## Features

### Splash Screen
Upon launching the app, users will be greeted with a visually appealing splash screen, creating a smooth transition into the app's main functionality.

### Dynamic Greetings
Based on the time of the day, Utask displays a dynamic greeting such as "Good Morning," "Good Evening," or "Good Night" to provide a personalized experience for the user. An accompanying flower icon adds a touch of charm.

### Task Page
The main page of Utask displays the user's tasks in a list view. Each task is represented using a ListTile widget for an organized and visually pleasing layout.

### Floating Action Button
Located at the bottom right corner of the screen, the floating action button (FAB) named "New Task" allows users to add new tasks with ease. Clicking on the FAB opens a showdialog screen where users can enter the task's title, description, and completion date.

### Task Editing and Deletion
To delete a task, the user can long-press on a specific task, triggering a bottom sheet to appear with all the details of the task. The bottom sheet asks for final confirmation before deleting the task. To edit a task, users can click on the task, and an edit box will open, allowing them to modify the existing task's information.

### Task Completion
Utask provides a way for users to mark tasks as complete. By clicking on the trailing icon on the right side of a task, a popup menu button/entry appears with two options: marking the task as complete or editing the task. If the user chooses to mark the task as complete, the task's title is struck through, and the background color changes, providing visual feedback on the task's completion status.

### Light and Dark Mode
Utask supports both light and dark modes. Users can switch between the modes by interacting with the app's app bar, which features a light icon when in dark mode and a dark icon when in light mode. The chosen mode provides a visually comfortable experience for users in different environments.

## Installation

1. Ensure that you have Flutter installed on your system. For information on installing Flutter, please refer to the official Flutter documentation: https://flutter.dev/docs/get-started/install

2. Clone the Utask repository from GitHub:

```
https://gitlab.com/hasan082/task-management-ostad.git
```

3. Change into the Utask directory:

```
cd Utask
```

4. Fetch the project dependencies by running the following command:

```
flutter pub get
```

5. Connect your device or start the emulator, then launch the app using the following command:

```
flutter run
```

## Contribution

Contributions to Utask are welcome! If you would like to contribute, please follow these steps:

1. Fork the repository on GitHub.

2. Create a new branch from the `main` branch.

3. Make your modifications and improvements.

4. Commit and push your changes to your forked repository.

5. Submit a pull request to the `main` branch of the original repository.

## Feedback and Support

If you encounter any issues while using Utask or have suggestions for improvements, please feel free to open an issue on the GitHub repository. We appreciate your feedback and will strive to address any concerns promptly.

## License

Utask is released under the [MIT License](https://opensource.org/license/mit/).
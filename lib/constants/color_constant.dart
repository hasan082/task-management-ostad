import 'package:flutter/material.dart';

//Common Color==========
Color whiteColor = Colors.white;


// Light Color=========
Color lightScafoldColor = Colors.grey.shade200;
Color mainThemeColor = const Color(0xFF43B6E5);
Color lightTextColor = const Color(0xFF333333);
Color lightCardBgColor = Colors.grey.shade200;
Color lightProgressBgColor = const Color(0xFFBADEFA);


// Dark Color=========

Color darkScafoldColor = const Color(0xFF303030);
Color darkThemeColor = const Color(0xFF333333);
Color darkTextColor = Colors.white;
Color darkCardBgColor = Colors.grey.shade200;
Color darkProgressBgColor = const Color(0xFFBADEFA);
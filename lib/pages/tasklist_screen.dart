import 'package:flutter/material.dart';
import 'package:task_management_ostad/utils/welcome_utility.dart';
import '../constants/color_constant.dart';
import '../controller/task_controller.dart';
import '../models/task_model.dart';
import '../theme_provider/thememanager.dart';
import '../widgets/reusable_textfield.dart';

class TaskListScreen extends StatefulWidget {
  const TaskListScreen({Key? key}) : super(key: key);

  @override
  State<TaskListScreen> createState() => _TaskListScreenState();
}

class _TaskListScreenState extends State<TaskListScreen> {
  List<Task> tasks = [];
  TextEditingController? titleCtrl;
  TextEditingController? descriptionCtrl;
  late TaskController taskController;
  final greeting = TimeUtils.getTimeOfDay();
  bool isDarkTheme = false;

  @override
  void initState() {
    super.initState();
    sortTasksByDeadline();
    taskController = TaskController(
      tasks: tasks,
      sortTasksByDeadline: sortTasksByDeadline,
    );
  }

  void sortTasksByDeadline() {
    tasks.sort((a, b) => a.deadline.compareTo(b.deadline));
  }


  @override
  Widget build(BuildContext context) {
    final themeColor = Theme
        .of(context)
        .colorScheme
        .primary;
    final themeManager = ThemeManager.of(context);



    return Scaffold(

      appBar: AppBar(
        title: const Text('UTask'),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: IconButton(
                icon: Icon(Icons.sunny,
                  color: !isDarkTheme ? darkScafoldColor : Colors.white,),
                onPressed: () {
                  setState(() {
                    isDarkTheme = !isDarkTheme;
                    themeManager?.toggleTheme(); // Toggle the theme
                  });
                },
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 16),
        child: ListView(
          children: [
            _buildGreetingRow(themeColor),
            const SizedBox(height: 10),
            _buildTaskList(themeColor),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => _showAddTaskDialog(context),
        label: const Text("New Task"),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );
  }

  Widget _buildGreetingRow(Color themeColor) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          'Hi Buddy, Good $greeting',
          style: const TextStyle(fontSize: 20),
        ),
        Image.asset(
          !isDarkTheme ? 'assets/images/flowerlight.png' : 'assets/images/flowerdark.png',
          // Replace with the actual image path
          width: 30,
          height: 23,
        ),
        const SizedBox(height: 20,)
      ],
    );
  }

  Widget _buildTaskList(Color themeColor) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const ScrollPhysics(),
      itemCount: tasks.length,
      itemBuilder: (context, index) {
        final task = tasks[index];
        return _buildTaskCard(themeColor, task);
      },
    );
  }


  Widget _buildTaskCard(Color themeColor, Task task) {
    return Card(
      color: task.completed ? Colors.grey[300] : Colors.grey[100],
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
      ),
      margin: const EdgeInsets.symmetric(vertical: 8),
      shadowColor: Colors.grey[300],
      elevation: 4,
      child: ListTile(
        isThreeLine: true,
        minVerticalPadding: 6,
        contentPadding: const EdgeInsets.symmetric(vertical: 3, horizontal: 10),
        title: Text(
          task.title,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            decoration: task.completed
                ? TextDecoration.lineThrough
                : TextDecoration.none,
          ),
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 5),
            Text(
              task.description,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            const SizedBox(height: 5),
            Text(task.deadline.toString().substring(0, 10)),
          ],
        ),
        leading: Image.asset(
          "assets/images/taskicon.png",
          height: 35,
        ),
        trailing: PopupMenuButton<String>(
          constraints: const BoxConstraints(
            maxWidth: 80,
          ),
          itemBuilder: (BuildContext context) =>
          <PopupMenuEntry<String>>[
            const PopupMenuItem<String>(
              value: 'edit',
              child: Icon(Icons.edit),
            ),
            PopupMenuItem<String>(
              value: 'done',
              child: Icon(task.completed ? Icons.undo : Icons.check),
            ),
          ],
          onSelected: (String value) {
            if (value == 'edit') {
              _showAddTaskDialog(context, task);
            } else if (value == 'done') {
              setState(() {
                task.completed = !task.completed;
              });
            }
          },
        ),
        onLongPress: () => _showDeleteBottomSheet(context, task),
      ),
    );
  }

  void _showAddTaskDialog(BuildContext context, [Task? task]) {
    final titleCtrl = TextEditingController(text: task?.title);
    final descriptionCtrl = TextEditingController(text: task?.description);
    DateTime deadline = task?.deadline ??
        DateTime.now().add(const Duration(days: 1));

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          title: Text(task == null ? 'Add Task' : 'Edit Task'),
          content: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ReusableTextField(controller: titleCtrl, labelText: 'Title'),
                  const SizedBox(height: 20),
                  ReusableTextField(
                    maxLines: 3,
                    controller: descriptionCtrl,
                    labelText: 'Description',
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text('Select Deadline'),
                      InkWell(
                        onTap: () async {
                          final selectedDate = await showDatePicker(
                            context: context,
                            initialDate: deadline,
                            firstDate: DateTime.now(),
                            lastDate: DateTime(2100),
                          );
                          if (selectedDate != null) {
                            setState(() {
                              deadline = selectedDate;
                            });
                          }
                        },
                        child: Text(deadline.toString().substring(0, 10)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          actions: [
            ElevatedButton(
              onPressed: () {
                final title = titleCtrl.text;
                final description = descriptionCtrl.text;
                if (title.isNotEmpty && description.isNotEmpty) {
                  if (task == null) {
                    setState(() {
                      tasks.add(
                        Task(
                          title: title,
                          description: description,
                          deadline: deadline,
                          completed: false,
                        ),
                      );
                    });
                  } else {
                    setState(() {
                      final index = tasks.indexOf(task);
                      tasks[index] = Task(
                        title: title,
                        description: description,
                        deadline: deadline,
                        completed: task.completed,
                      );
                    });
                  }
                  sortTasksByDeadline();
                  Navigator.of(context).pop();
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      backgroundColor: Color(0xFF43B6E5),
                      content: Text("Task Empty. Add All Fields"),
                      duration: Duration(seconds: 2),
                    ),
                  );
                }
              },
              child: Text(task == null ? 'Save' : 'Update'),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Cancel'),
            ),
          ],
        );
      },
    );
  }


  void _showDeleteBottomSheet(BuildContext context, Task task) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          padding: const EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Delete Task',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 10),
              Text('Title: ${task.title}'),
              const SizedBox(height: 5),
              Text('Description: ${task.description}'),
              const SizedBox(height: 5),
              Text('Deadline: ${task.deadline.toString().substring(0, 10)}'),
              const SizedBox(height: 15),
              const Text(
                'Are you sure you want to delete this task?',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        tasks.remove(task);
                      });
                      Navigator.of(context).pop();
                    },
                    child: const Text('Delete'),
                  ),
                  const SizedBox(width: 10),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('Cancel'),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }


}

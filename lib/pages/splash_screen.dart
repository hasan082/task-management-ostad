import 'dart:async';

import 'package:flutter/material.dart';
import 'package:task_management_ostad/constants/color_constant.dart';
import 'package:task_management_ostad/pages/tasklist_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(const Duration(seconds: 4), () {
      Navigator.pushAndRemoveUntil(context,
          MaterialPageRoute(builder: (context) {
        return const TaskListScreen();
      }), (route) => false);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainThemeColor,
      body: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: 60,
                child: Image.asset(
                  "assets/images/taskicon.png",
                  width: 90,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const Text(
                "UTask",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Colors.white),
              ),
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                width: 150,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10), // Set the border radius
                  child: const LinearProgressIndicator(
                    minHeight: 4,
                    // Other properties...
                  ),
                )

              )
            ],
          ),
        ),
      ),
    );
  }
}

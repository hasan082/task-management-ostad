import 'package:flutter/material.dart';
import 'package:task_management_ostad/constants/color_constant.dart';
import 'package:task_management_ostad/pages/splash_screen.dart';
import 'package:task_management_ostad/theme_provider/theme_provider.dart';
import 'package:task_management_ostad/theme_provider/thememanager.dart';

void main() => runApp(const TaskManagementApp());

class TaskManagementApp extends StatefulWidget {
  const TaskManagementApp({Key? key}) : super(key: key);

  @override
  State<TaskManagementApp> createState() => _TaskManagementAppState();
}

class _TaskManagementAppState extends State<TaskManagementApp> {

  ThemeData _currentTheme = CustomTheme.lightTheme;

  void _toggleTheme() {
    setState(() {
      _currentTheme = _currentTheme == CustomTheme.lightTheme
          ? CustomTheme.darkTheme
          : CustomTheme.lightTheme;
    });
  }

  @override
  @override
  Widget build(BuildContext context) {
    return ThemeManager(
      themeData: _currentTheme,
      toggleTheme: _toggleTheme,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: _currentTheme,
        home: const SplashScreen(),
      ),
    );
  }
// Widget build(BuildContext context) {
//   return MaterialApp(
//     debugShowCheckedModeBanner: false,
//     title: 'UTask',
//     home: const SplashScreen(),
//   );
// }
}

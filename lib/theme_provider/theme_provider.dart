import 'package:flutter/material.dart';

import '../constants/color_constant.dart';

class CustomTheme {


  static ThemeData lightTheme = ThemeData.light().copyWith(
    primaryColor: mainThemeColor,
    appBarTheme: AppBarTheme(
      color: mainThemeColor, // Set the background color of AppBar in light mode
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: mainThemeColor,
    ),
  );


  static ThemeData darkTheme = ThemeData.dark().copyWith(
    primaryColor: darkScafoldColor,
      listTileTheme: ListTileThemeData(
        tileColor: darkThemeColor,
      ),
    appBarTheme: AppBarTheme(
      elevation: 5,
      color: darkThemeColor, // Set the background color of AppBar in light mode
    ),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: darkThemeColor,
      foregroundColor: whiteColor
    ),
      // elevatedButtonTheme: ElevatedButtonThemeData(
      //   // Update the elevated button styles as per your requirements
      //   style: ButtonStyle(
      //     backgroundColor: MaterialStateProperty.all<Color>(darkThemeColor),
      //     textStyle: MaterialStateProperty.all<TextStyle>(
      //       TextStyle(color: whiteColor),
      //     ),
      //   ),
      // ),
  );






// // Light Theme setup ==================
  //
  // static ThemeData lightTheme = ThemeData(
  //   primaryColor: mainThemeColor,
  //   scaffoldBackgroundColor: lightScafoldColor,
  //   floatingActionButtonTheme: FloatingActionButtonThemeData(
  //     foregroundColor: whiteColor,
  //   ),
  //   colorScheme: ColorScheme.light(
  //     primary: mainThemeColor,
  //     secondary: Colors.grey.shade200,
  //   ),
  //   buttonTheme: ButtonThemeData(
  //     // Update the button styles as per your requirements
  //     buttonColor: mainThemeColor,
  //     textTheme: ButtonTextTheme.primary,
  //   ),
  //   elevatedButtonTheme: ElevatedButtonThemeData(
  //     // Update the elevated button styles as per your requirements
  //     style: ButtonStyle(
  //       backgroundColor: MaterialStateProperty.all<Color>(mainThemeColor),
  //       textStyle: MaterialStateProperty.all<TextStyle>(
  //         TextStyle(color: whiteColor),
  //       ),
  //     ),
  //   ),
  //   cardTheme: CardTheme(
  //     // Update the card styles as per your requirements
  //     color: lightCardBgColor,
  //   ),
  //   listTileTheme: ListTileThemeData(
  //     // Update the list tile styles as per your requirements
  //     tileColor: whiteColor,
  //   ),
  //   progressIndicatorTheme: ProgressIndicatorThemeData(
  //     color: lightProgressBgColor,
  //     linearTrackColor: whiteColor,
  //   ),
  // );
  //
  // //Dark Theme Setup ===================
  //
  // static ThemeData darkTheme = ThemeData(
  //   primaryColor: darkThemeColor,
  //   scaffoldBackgroundColor: darkScafoldColor,
  //   floatingActionButtonTheme: const FloatingActionButtonThemeData(
  //     foregroundColor: Colors.white,
  //     backgroundColor: Colors.black,
  //   ),
  //   colorScheme: ColorScheme.dark(
  //     primary: darkThemeColor,
  //   ),
  //   buttonTheme: ButtonThemeData(
  //     // Update the button styles as per requirements
  //     buttonColor: darkThemeColor,
  //     textTheme: ButtonTextTheme.primary,
  //   ),
  //   elevatedButtonTheme: ElevatedButtonThemeData(
  //     // Update the elevated button styles as per requirements
  //     style: ButtonStyle(
  //       backgroundColor: MaterialStateProperty.all<Color>(darkThemeColor),
  //       textStyle: MaterialStateProperty.all<TextStyle>(
  //         const TextStyle(color: Colors.white),
  //       ),
  //     ),
  //   ),
  //   cardTheme: const CardTheme(
  //     // Update the card styles as per requirements
  //     color: Colors.black,
  //     elevation: 2,
  //   ),
  //   listTileTheme: const ListTileThemeData(
  //     // Update the list tile styles as per requirements
  //     tileColor: Color(0xFF333333),
  //   ),
  //   progressIndicatorTheme: ProgressIndicatorThemeData(
  //     color: darkProgressBgColor,
  //     linearTrackColor: whiteColor,
  //   ),
  // );






}


// textTheme: TextTheme(
// Update the text styles as per your requirements
// headlineLarge: TextStyle(
//     color: lightTextColor, fontSize: 24, fontWeight: FontWeight.bold),
// headlineMedium: TextStyle(
//     color: lightTextColor, fontSize: 20, fontWeight: FontWeight.w600),
// headlineSmall: TextStyle(
//     color: lightTextColor, fontSize: 18, fontWeight: FontWeight.w500),
// bodyLarge: TextStyle(
//     color: lightTextColor, fontSize: 17,),
// bodyMedium: TextStyle(
//     color: lightTextColor, fontSize: 16,),
// bodySmall: TextStyle(
//     color: lightTextColor, fontSize: 15,),
// Add more text styles as needed
// ),
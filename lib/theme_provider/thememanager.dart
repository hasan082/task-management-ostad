import 'package:flutter/material.dart';

class ThemeManager extends InheritedWidget {
  final ThemeData themeData;
  final Function toggleTheme;

  const ThemeManager({super.key,
    required this.themeData,
    required this.toggleTheme,
    required Widget child,
  }) : super(child: child);

  static ThemeManager? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<ThemeManager>();
  }

  @override
  bool updateShouldNotify(covariant ThemeManager oldWidget) {
    return themeData != oldWidget.themeData;
  }
}

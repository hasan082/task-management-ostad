class Task {
  late final String title;
  late final String description;
  late final DateTime deadline;
  bool completed;

  Task({
    required this.title,
    required this.description,
    required this.deadline,
    this.completed = false,
  });
}
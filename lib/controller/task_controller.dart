import 'package:flutter/material.dart';
import 'package:task_management_ostad/models/task_model.dart';
import '../widgets/addtaskdialoge_widget.dart';

class TaskController {

  final List<Task> tasks;
  final Function sortTasksByDeadline;

  TaskController({required this.tasks, required this.sortTasksByDeadline});

  void showAddTaskDialog(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return AddTaskDialog(
          onSave: (String title, String description, DateTime deadline) {
            tasks.add(
              Task(
                title: title,
                description: description,
                deadline: deadline,
                completed: false,
              ),
            );
            sortTasksByDeadline();
          },
        );
      },
    );
  }





}

import 'package:flutter/material.dart';

class TimeUtils {
  static String getTimeOfDay() {
    final now = DateTime.now();
    final currentTime = TimeOfDay.fromDateTime(now);

    if (currentTime.hour >= 5 && currentTime.hour < 12) {
      return 'Morning';
    } else if (currentTime.hour >= 12 && currentTime.hour < 17) {
      return 'Afternoon';
    } else if (currentTime.hour >= 17 && currentTime.hour < 21) {
      return 'Evening';
    } else {
      return 'Night';
    }
  }
}

import 'package:flutter/material.dart';
import 'package:task_management_ostad/widgets/reusable_textfield.dart';

class AddTaskDialog extends StatefulWidget {
  final Function(String title, String description, DateTime deadline) onSave;

  const AddTaskDialog({Key? key, required this.onSave}) : super(key: key);

  @override
  State<AddTaskDialog> createState() => _AddTaskDialogState();
}

class _AddTaskDialogState extends State<AddTaskDialog> {
  late TextEditingController titleCtrl;
  late TextEditingController descriptionCtrl;
  late DateTime deadline;

  @override
  void initState() {
    super.initState();
    titleCtrl = TextEditingController();
    descriptionCtrl = TextEditingController();
    deadline = DateTime.now().add(const Duration(days: 1));
  }

  @override
  void dispose() {
    titleCtrl.dispose();
    descriptionCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.zero,
      title: const Text('Add Task'),
      content: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ReusableTextField(
                controller: titleCtrl,
                labelText: 'Title',
              ),
              const SizedBox(height: 20),
              ReusableTextField(
                maxLines: 3,
                controller: descriptionCtrl,
                labelText: 'Description',
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Select Deadline'),
                  InkWell(
                      onTap: () async {
                        final selectDate = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime.now(),
                            lastDate: DateTime(2100));
                        if (selectDate != null) {
                          deadline = selectDate;
                        }
                      },
                      child: Text(deadline.toString().substring(0, 10))),
                ],
              ),
            ],
          ),
        ),
      ),
      actions: [
        ElevatedButton(
          onPressed: () {
            final title = titleCtrl.text;
            final description = descriptionCtrl.text;

            if (title.isNotEmpty && description.isNotEmpty) {
              widget.onSave(title, description, deadline);
              Navigator.of(context).pop();
            } else {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  backgroundColor: Color(0xFF43B6E5),
                  content: Text("Task Empty. Add All Fields"),
                  duration: Duration(seconds: 2),
                ),
              );
            }
          },
          child: const Text('Save'),
        ),
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text('Cancel'),
        ),
      ],
    );
  }
}
